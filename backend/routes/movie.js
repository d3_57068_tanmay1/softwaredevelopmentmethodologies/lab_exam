const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.get("/", (request, response) => {
  const connection = db.openConnection();
  const statement = `SELECT * FROM movie `;
  connection.query(statement, (error, result) => {
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
    connection.end();
  });
});

router.post("/add", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;
  const connection = db.openConnection();
  const statement = `insert into movie (movie_title,movie_release_date,movie_time,director_name) values ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`;
  connection.query(statement, (error, result) => {
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
    connection.end();
  });
});

router.put("/update/:id", (request, response) => {
  const { id } = request.params;
  const { movie_release_date, movie_time } = request.body;
  const connection = db.openConnection();
  const statement = `update movie set 
  movie_release_date='${movie_release_date}',
  movie_time='${movie_time}' 
  WHERE movie_id=${id}`;
  connection.query(statement, (error, result) => {
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
    connection.end();
  });
});

router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const connection = db.openConnection();
  const statement = `DELETE FROM movie WHERE movie_id=${id}`;
  connection.query(statement, (error, result) => {
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
    connection.end();
  });
});

module.exports = router;
